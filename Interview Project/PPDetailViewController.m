//
//  PPDetailViewController.m
//  Interview Project
//
//  Created by Jens Andersson on 2014-01-29.
//  Copyright (c) 2014 Projectplace. All rights reserved.
//

#import "PPDetailViewController.h"
#import "UIImage+animatedGIF.h"



@interface PPDetailViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *detailDescriptionWebView;

@property (nonatomic, copy) void (^loadImageBlock)();

@end

@implementation PPDetailViewController
@synthesize detailDescriptionWebView;

#pragma mark - Managing the detail item

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.detailDescriptionWebView loadHTMLString:self.post[@"caption"] baseURL:nil];
    
    
    NSDictionary *photo = _post[@"photos"][0];
    NSString *smallUrl = [[photo valueForKeyPath:@"alt_sizes.@firstObject"] objectForKey:@"url"];
    
     NSLog(@"Value of photos is %@",photo);
    NSURL * url = 		[NSURL URLWithString:smallUrl];
    
    //self.imageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [detailDescriptionWebView loadRequest:urlRequest];
    
   
    
    
 //    self.detailDescriptionLabel.text = self.post[@"caption"];
    
    
    
    

    /*typeof(self) weakSelf = self;
    self.loadImageBlock = ^{
        NSDictionary *photo = _post[@"photos"][0];
        NSString *smallUrl = [[photo valueForKeyPath:@"alt_sizes.@firstObject"] objectForKey:@"url"];
        
        [weakSelf setImageWithURL:[NSURL URLWithString:smallUrl]];
    };
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), self.loadImageBlock);*/
    
}

- (void)setImageWithURL:(NSURL *)url {
    
    
    
    UIImage *image = [UIImage animatedImageWithAnimatedGIFURL:url];//Get animated image from gif url
    //self.imageView.image=firstAnimation;
   
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
}

@end
