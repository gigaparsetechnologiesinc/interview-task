//
//  PPMasterViewController.m
//  Interview Project
//
//  Created by Jens Andersson on 2014-01-29.
//  Copyright (c) 2014 Projectplace. All rights reserved.
//

#import "PPMasterViewController.h"
#import "PPDetailViewController.h"
#import "AFNetworking.h"
#import "NSString+StipHTML.h"
#import "UIImageView+Gray.h"

@interface PPMasterViewController () {
}

@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic) PPDetailViewController *detailView;
@property (nonatomic, copy) void (^successCallback)(NSArray *posts);
@property (nonatomic) NSMutableDictionary *cachedImages;

@property (nonatomic) UILabel *descriptionLabel;

// Used for tracking.
@property (nonatomic) NSMutableArray *clickedTitles;

@end

@implementation PPMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.clickedTitles = [NSMutableArray array];
    self.cachedImages = [NSMutableDictionary dictionary];

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(fetchData)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    
    void (^successCallback)(NSArray *posts) = ^(NSArray *posts) {
        self.posts = posts.mutableCopy;
        //Ankkit  here is using NSUInteger aND ITS CAST IS unsigned long
        NSLog(@"count %lu",(unsigned long)self.posts.count);
        [self.tableView reloadData];
    };
    
    self.successCallback = successCallback;
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"cachedPosts"];
    if(data != nil){
    NSArray *cachedPosts = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    self.successCallback(cachedPosts);
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    //Ankit
    /*[self fetchData];
    [self.tableView reloadData];*/
    [AsyncImageView enableGrayification];
}

- (void)setUpHeaderWithData:(NSDictionary *)info {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectInset(headerView.frame, 10, 10)];
    self.descriptionLabel.numberOfLines = 4;
    self.descriptionLabel.text = [info[@"description"] stringByStrippingHTML];
    self.navigationItem.title = info[@"title"];
    
    [headerView addSubview:self.descriptionLabel];
    
    self.tableView.tableHeaderView = headerView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchData {
    //NSString *urlStr = @"https://api.tumblr.com/v2/blog/iheartcatgifs.tumblr.com/posts?api_key=yQQKdUWSjfvQ9h6rp1nnpuZrXuYRzOVXSQjuw0vJsAzXBh0buv&limit=50";
    NSURL *url = [NSURL URLWithString:@"https://api.tumblr.com/v2/blog/iheartcatgifs.tumblr.com/posts?api_key=yQQKdUWSjfvQ9h6rp1nnpuZrXuYRzOVXSQjuw0vJsAzXBh0buv&limit=50"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSDictionary *blogInfo = [JSON valueForKeyPath:@"response.blog"];
        
        NSLog(@"value of dictionary is %@",blogInfo);
        [self setUpHeaderWithData:blogInfo];
        
        NSArray *posts = [JSON valueForKeyPath:@"response.posts"];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:posts];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"cachedPosts"];
        self.successCallback(posts);
    } failure:nil];
    [operation start];
    
    
    /*AFHTTPRequestOperationManager *opManager = [AFHTTPRequestOperationManager manager];
    opManager.responseSerializer = [AFJSONResponseSerializer serializer];
    [opManager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"response = %@",responseObject);
        NSDictionary *blogInfo = [responseObject valueForKeyPath:@"response.blog"];
        [self setUpHeaderWithData:blogInfo];
        
        NSArray *posts = [responseObject valueForKeyPath:@"response.posts"];
         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:posts];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"cachedPosts"];
         self.successCallback(posts);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error = %@",error.description);
    }];*/
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"value of array is %@",self.posts);
    NSLog(@"number of row in section is %lu",(unsigned long)self.posts.count);
    return self.posts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    

    NSDictionary *post = self.posts[indexPath.row];
    
    NSLog(@"value of dictionary is %@",post);
    
    /*NSString *caption = [post[@"caption"] stringByStrippingHTML];
    
    if (caption.length == 0)
        caption = @"<NO CAPTION>";
    UIWebView *webView = (UIWebView *)[cell.contentView viewWithTag:1];
    [webView loadHTMLString:caption baseURL:nil];*/
    
    
    UILabel *lblblogname = [[UILabel alloc] initWithFrame:CGRectMake(100, 15, 200, 50)];
    lblblogname.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:lblblogname];
    
    lblblogname.text =post[@"blog_name"];
    
   // cell.textLabel.frame =CGRectMake(25, 0, 40, 40);
    
    NSDictionary *photo = post[@"photos"][0];
    NSString *smallUrl = [[photo valueForKeyPath:@"alt_sizes.@firstObject"] objectForKey:@"url"];
    
    // Fetch images on seperate thread
    UIImage *cachedImage = self.cachedImages[post[@"id"]];
    if (cachedImage){
        NSData *imgData = [self getImageDataFromDocumentDirectoryFromImageName:smallUrl];
        UIImage *img = [UIImage imageWithData:imgData];
        ((AsyncImageView *)[cell.contentView viewWithTag:2]).image = img;
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            __block NSURL *url = [NSURL URLWithString:smallUrl];
            __block NSData *imageData = [NSData dataWithContentsOfURL:url];
            if (!imageData) return;
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *image = [UIImage imageWithData:imageData];
                    self.cachedImages[post[@"id"]] = image;
                    ((AsyncImageView *)[cell.contentView viewWithTag:2]).image = image;
                    [self writeData:imageData ToDicumentDirectoryWithName:url.absoluteString];
                });
           
            }
        });
    }
    cell.detailTextLabel.text=@"Ankit";
    
    NSLog(@"value of table videw iframe is %@",cell.detailTextLabel);
    NSLog(@"value of table videw text label is %@",cell.textLabel);
    

    // Is this the most shared post?
    /*NSNumber *noteCount = post[@"note_count"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Notes: %@", noteCount];
    BOOL isTheMostShared = YES;
    for (NSDictionary *p in self.posts) {
        if ([p[@"note_count"] compare:noteCount] == NSOrderedAscending) {
            isTheMostShared = NO;
        }
    }
    if (isTheMostShared) {
        cell.textLabel.textColor = [UIColor greenColor];
    }*/
    
    return cell;
}

- (void)writeData:(NSData *)imgData ToDicumentDirectoryWithName:(NSString *)name{
    NSString *ddPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = name;
    NSString *filePath = [ddPath stringByAppendingPathComponent:fileName];
    [imgData writeToFile:filePath atomically:YES];
}

- (NSData *)getImageDataFromDocumentDirectoryFromImageName:(NSString *)imgName{
    NSString *ddPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fileName = imgName;
    NSString *filePath = [ddPath stringByAppendingPathComponent:fileName];
    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:filePath]];
    return imgData;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

#pragma mark - Master delegate

- (void)reloadDataFromAPI {
    [self fetchData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *post = self.posts[indexPath.row];
        NSString *title = post[@"source_title"];
        
        // Add each clicked source to this array for tracking purposes
        if (title!=nil) { //Check post title is not nil
            [self.clickedTitles addObject:title];

        }
        
        self.detailView = [segue destinationViewController];
        self.detailView.masterDelegate = self;
        [self.detailView setPost:post];
        
        // Remove cat from list when taping on it.
        // Don't want to see that cat again.
        //[self.posts removeObjectAtIndex:indexPath.row];
    }
}

- (void)dealloc {
    
}

@end
